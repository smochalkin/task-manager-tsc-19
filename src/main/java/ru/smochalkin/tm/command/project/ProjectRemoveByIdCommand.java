package ru.smochalkin.tm.command.project;

import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.exception.entity.ProjectNotFoundException;
import ru.smochalkin.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractCommand {

    @Override
    public String name() {
        return "project-remove-by-id";
    }

    @Override
    public String description() {
        return "Remove project by id.";
    }

    @Override
    public void execute() {
        System.out.print("Enter project id: ");
        String projectId = TerminalUtil.nextLine();
        if (!serviceLocator.getProjectTaskService().isProjectId(projectId)) {
            throw new ProjectNotFoundException();
        }
        serviceLocator.getProjectTaskService().removeProjectById(projectId);
        System.out.println("[OK]");
    }

}
