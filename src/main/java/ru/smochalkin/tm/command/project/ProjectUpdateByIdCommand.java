package ru.smochalkin.tm.command.project;

import ru.smochalkin.tm.command.AbstractProjectCommand;
import ru.smochalkin.tm.util.TerminalUtil;

public final class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-update-by-id";
    }

    @Override
    public String description() {
        return "Update project by id.";
    }

    @Override
    public void execute() {
        System.out.print("Enter id: ");
        String id = TerminalUtil.nextLine();
        serviceLocator.getProjectService().findById(id);
        System.out.print("Enter new name: ");
        String name = TerminalUtil.nextLine();
        System.out.print("Enter new description: ");
        String desc = TerminalUtil.nextLine();
        serviceLocator.getProjectService().updateById(id, name, desc);
    }

}
