package ru.smochalkin.tm.command.project;

import ru.smochalkin.tm.command.AbstractProjectCommand;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.util.TerminalUtil;

public final class ProjectStartByIdCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-start-by-id";
    }

    @Override
    public String description() {
        return "Start project by id.";
    }

    @Override
    public void execute() {
        System.out.print("Enter id: ");
        String id = TerminalUtil.nextLine();
        serviceLocator.getProjectService().findById(id);
        serviceLocator.getProjectService().updateStatusById(id, Status.IN_PROGRESS);
    }

}
