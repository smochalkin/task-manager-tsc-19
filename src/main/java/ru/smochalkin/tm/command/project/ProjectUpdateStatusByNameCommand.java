package ru.smochalkin.tm.command.project;

import ru.smochalkin.tm.command.AbstractProjectCommand;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.exception.system.StatusNotFoundException;
import ru.smochalkin.tm.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectUpdateStatusByNameCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-status-update-by-name";
    }

    @Override
    public String description() {
        return "Update project status by name.";
    }

    @Override
    public void execute() {
        System.out.print("Enter name: ");
        String name = TerminalUtil.nextLine();
        serviceLocator.getProjectService().findByName(name);
        System.out.println("Enter new status from list:");
        System.out.println(Arrays.toString(Status.values()));
        String statusName = TerminalUtil.nextLine();
        Status status;
        try {
            status = Status.valueOf(statusName);
        } catch (IllegalArgumentException e) {
            throw new StatusNotFoundException();
        }
        serviceLocator.getProjectService().updateStatusByName(name, status);
    }

}
