package ru.smochalkin.tm.command.project;

import ru.smochalkin.tm.command.AbstractProjectCommand;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.util.TerminalUtil;

public final class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-complete-by-id";
    }

    @Override
    public String description() {
        return "Complete project by id.";
    }

    @Override
    public void execute() {
        System.out.print("Enter id: ");
        String id = TerminalUtil.nextLine();
        serviceLocator.getProjectService().findById(id);
        serviceLocator.getProjectService().updateStatusById(id, Status.COMPLETED);
    }

}
