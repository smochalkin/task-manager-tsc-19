package ru.smochalkin.tm.command.project;

import ru.smochalkin.tm.command.AbstractProjectCommand;
import ru.smochalkin.tm.util.TerminalUtil;

public final class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-update-by-index";
    }

    @Override
    public String description() {
        return "Update project by index.";
    }

    @Override
    public void execute() {
        System.out.print("Enter index: ");
        Integer index = TerminalUtil.nextInt();
        index--;
        serviceLocator.getProjectService().findByIndex(index);
        System.out.print("Enter new name: ");
        String name = TerminalUtil.nextLine();
        System.out.print("Enter new description: ");
        String desc = TerminalUtil.nextLine();
        serviceLocator.getProjectService().updateByIndex(index, name, desc);
    }

}
