package ru.smochalkin.tm.command.project;

import ru.smochalkin.tm.command.AbstractProjectCommand;
import ru.smochalkin.tm.model.Project;
import ru.smochalkin.tm.util.TerminalUtil;

public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-show-by-id";
    }

    @Override
    public String description() {
        return "Show project by id.";
    }

    @Override
    public void execute() {
        System.out.print("Enter id: ");
        String id = TerminalUtil.nextLine();
        Project project = serviceLocator.getProjectService().findById(id);
        showProject(project);
    }

}
