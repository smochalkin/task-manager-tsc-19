package ru.smochalkin.tm.command.project;

import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.exception.entity.ProjectNotFoundException;
import ru.smochalkin.tm.util.TerminalUtil;

public final class ProjectRemoveByIndexCommand extends AbstractCommand {

    @Override
    public String name() {
        return "project-remove-by-index";
    }

    @Override
    public String description() {
        return "Remove project by index.";
    }

    @Override
    public void execute() {
        System.out.print("Enter project index: ");
        Integer index = TerminalUtil.nextInt();
        if (!serviceLocator.getProjectTaskService().isProjectIndex(index)) {
            throw new ProjectNotFoundException();
        }
        serviceLocator.getProjectTaskService().removeProjectByIndex(index);
        System.out.println("[OK]");
    }

}
