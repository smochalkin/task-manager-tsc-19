package ru.smochalkin.tm.command.project;

import ru.smochalkin.tm.command.AbstractProjectCommand;
import ru.smochalkin.tm.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-create";
    }

    @Override
    public String description() {
        return "Create new project.";
    }

    @Override
    public void execute() {
        System.out.println("[CREATE PROJECT]");
        System.out.print("Enter name: ");
        String name = TerminalUtil.nextLine();
        System.out.print("Enter description: ");
        String description = TerminalUtil.nextLine();
        serviceLocator.getProjectService().create(name, description);
        System.out.println("[OK]");
    }

}
