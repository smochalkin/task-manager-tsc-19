package ru.smochalkin.tm.command.project;

import ru.smochalkin.tm.command.AbstractProjectCommand;
import ru.smochalkin.tm.model.Project;
import ru.smochalkin.tm.util.TerminalUtil;

public final class ProjectShowByNameCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-show-by-name";
    }

    @Override
    public String description() {
        return "Show project by name.";
    }

    @Override
    public void execute() {
        System.out.print("Enter name: ");
        String name = TerminalUtil.nextLine();
        Project project = serviceLocator.getProjectService().findByName(name);
        showProject(project);
    }

}
