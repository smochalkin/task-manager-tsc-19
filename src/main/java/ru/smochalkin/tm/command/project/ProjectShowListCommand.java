package ru.smochalkin.tm.command.project;

import ru.smochalkin.tm.api.service.IProjectService;
import ru.smochalkin.tm.command.AbstractProjectCommand;
import ru.smochalkin.tm.enumerated.Sort;
import ru.smochalkin.tm.exception.system.SortNotFoundException;
import ru.smochalkin.tm.model.Project;
import ru.smochalkin.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectShowListCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-list";
    }

    @Override
    public String description() {
        return "Display project list.";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        System.out.println("Enter sort option from list:");
        System.out.println(Arrays.toString(Sort.values()));
        List<Project> projects;
        String sortName = TerminalUtil.nextLine();
        if (sortName == null || sortName.isEmpty()) {
            projects = serviceLocator.getProjectService().findAll();
        } else {
            Sort sort;
            try {
                sort = Sort.valueOf(sortName);
            } catch (IllegalArgumentException e) {
                throw new SortNotFoundException();
            }
            System.out.println(sort.getDisplayName());
            projects = serviceLocator.getProjectService().findAll(sort.getComparator());
        }
        int index = 1;
        for (Project project : projects) {
            System.out.println(index++ + ". " + project);
        }
        System.out.println("[OK]");
    }

}
