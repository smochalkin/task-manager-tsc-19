package ru.smochalkin.tm.command.project;

import ru.smochalkin.tm.command.AbstractProjectCommand;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.util.TerminalUtil;

public final class ProjectCompleteByNameCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-complete-by-name";
    }

    @Override
    public String description() {
        return "Complete project by name.";
    }

    @Override
    public void execute() {
        System.out.print("Enter name: ");
        String name = TerminalUtil.nextLine();
        serviceLocator.getProjectService().findByName(name);
        serviceLocator.getProjectService().updateStatusByName(name, Status.COMPLETED);
    }

}
