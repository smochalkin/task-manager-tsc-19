package ru.smochalkin.tm.command.project;

import ru.smochalkin.tm.command.AbstractProjectCommand;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.util.TerminalUtil;

public final class ProjectCompleteByIndexCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-complete-by-index";
    }

    @Override
    public String description() {
        return "Complete project by index.";
    }

    @Override
    public void execute() {
        System.out.print("Enter index: ");
        Integer index = TerminalUtil.nextInt();
        index--;
        serviceLocator.getProjectService().findByIndex(index);
        serviceLocator.getProjectService().updateStatusByIndex(index, Status.COMPLETED);
    }

}
