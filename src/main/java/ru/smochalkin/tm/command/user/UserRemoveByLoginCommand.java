package ru.smochalkin.tm.command.user;

import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.util.TerminalUtil;

public class UserRemoveByLoginCommand extends AbstractCommand {

    @Override
    public String name() {
        return "user-remove-by-login";
    }

    @Override
    public String description() {
        return "Remove a user by login.";
    }

    @Override
    public void execute() {
        System.out.println("Enter login:");
        String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().removeByLogin(login);
    }

}
