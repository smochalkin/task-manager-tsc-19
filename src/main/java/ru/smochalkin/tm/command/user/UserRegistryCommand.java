package ru.smochalkin.tm.command.user;

import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.util.TerminalUtil;

public class UserRegistryCommand extends AbstractCommand {

    @Override
    public String name() {
        return "user-registry";
    }

    @Override
    public String description() {
        return "User registry";
    }

    @Override
    public void execute() {
        System.out.println("Enter login:");
        String login = TerminalUtil.nextLine();
        System.out.println("Enter email:");
        String email = TerminalUtil.nextLine();
        System.out.println("Enter password:");
        String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().registry(login, password, email);
    }

}
