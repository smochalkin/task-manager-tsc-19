package ru.smochalkin.tm.command.task;

import ru.smochalkin.tm.command.AbstractTaskCommand;
import ru.smochalkin.tm.model.Task;
import ru.smochalkin.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-show-by-index";
    }

    @Override
    public String description() {
        return "Show task by index.";
    }

    @Override
    public void execute() {
        System.out.print("Enter index: ");
        Integer index = TerminalUtil.nextInt();
        Task task = serviceLocator.getTaskService().findByIndex(--index);
        showTask(task);
    }

}
