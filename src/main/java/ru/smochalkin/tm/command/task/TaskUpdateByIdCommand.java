package ru.smochalkin.tm.command.task;

import ru.smochalkin.tm.command.AbstractTaskCommand;
import ru.smochalkin.tm.util.TerminalUtil;

public final class TaskUpdateByIdCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-update-by-id";
    }

    @Override
    public String description() {
        return "Update task by id.";
    }

    @Override
    public void execute() {
        System.out.print("Enter id: ");
        String id = TerminalUtil.nextLine();
        serviceLocator.getTaskService().findById(id);
        System.out.print("Enter new name: ");
        String name = TerminalUtil.nextLine();
        System.out.print("Enter new description: ");
        String desc = TerminalUtil.nextLine();
        serviceLocator.getTaskService().updateById(id, name, desc);
    }

}
