package ru.smochalkin.tm.command.task;

import ru.smochalkin.tm.command.AbstractTaskCommand;
import ru.smochalkin.tm.util.TerminalUtil;

public final class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-update-by-index";
    }

    @Override
    public String description() {
        return "Update task by index.";
    }

    @Override
    public void execute() {
        System.out.print("Enter index: ");
        Integer index = TerminalUtil.nextInt();
        index--;
        serviceLocator.getTaskService().findByIndex(index);
        System.out.print("Enter new name: ");
        String name = TerminalUtil.nextLine();
        System.out.print("Enter new description: ");
        String desc = TerminalUtil.nextLine();
        serviceLocator.getTaskService().updateByIndex(index, name, desc);
    }

}
