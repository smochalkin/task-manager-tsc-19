package ru.smochalkin.tm.command.task;

import ru.smochalkin.tm.command.AbstractTaskCommand;
import ru.smochalkin.tm.enumerated.Sort;
import ru.smochalkin.tm.exception.system.SortNotFoundException;
import ru.smochalkin.tm.model.Task;
import ru.smochalkin.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskShowListCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-list";
    }

    @Override
    public String description() {
        return "Display list of tasks.";
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        System.out.println("Enter sort option from list:");
        System.out.println(Arrays.toString(Sort.values()));
        List<Task> tasks;
        String sortName = TerminalUtil.nextLine();
        if (sortName == null || sortName.isEmpty()) {
            tasks = serviceLocator.getTaskService().findAll();
        } else {
            Sort sort;
            try {
                sort = Sort.valueOf(sortName);
            } catch (IllegalArgumentException e) {
                throw new SortNotFoundException();
            }
            System.out.println(sort.getDisplayName());
            tasks = serviceLocator.getTaskService().findAll(sort.getComparator());
        }
        int index = 1;
        for (Task task : tasks) {
            System.out.println(index++ + ". " + task);
        }
        System.out.println("[OK]");
    }

}
