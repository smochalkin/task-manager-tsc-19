package ru.smochalkin.tm.command.task;

import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.exception.entity.ProjectNotFoundException;
import ru.smochalkin.tm.model.Task;
import ru.smochalkin.tm.util.TerminalUtil;

import java.util.List;

public final class TaskShowListByProjectIdCommand extends AbstractCommand {

    @Override
    public String name() {
        return "task-show-by-project-id";
    }

    @Override
    public String description() {
        return "Show tasks by project id.";
    }

    @Override
    public void execute() {
        System.out.print("Enter project id: ");
        String projectId = TerminalUtil.nextLine();
        if (!serviceLocator.getProjectTaskService().isProjectId(projectId)) {
            throw new ProjectNotFoundException();
        }
        System.out.println("[TASK LIST BY PROJECT ID = "+ projectId + "]");
        List<Task> tasks = serviceLocator.getProjectTaskService().findTasksByProjectId(projectId);
        for (Task task : tasks) {
            System.out.println(task);
        }
        System.out.println("[OK]");
    }

}
