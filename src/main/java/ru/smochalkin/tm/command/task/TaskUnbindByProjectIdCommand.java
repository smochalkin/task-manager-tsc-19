package ru.smochalkin.tm.command.task;

import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.exception.entity.ProjectNotFoundException;
import ru.smochalkin.tm.exception.entity.TaskNotFoundException;
import ru.smochalkin.tm.util.TerminalUtil;

public final class TaskUnbindByProjectIdCommand extends AbstractCommand {

    @Override
    public String name() {
        return "task-unbind-by-project-id";
    }

    @Override
    public String description() {
        return "Unbind task to project by id.";
    }

    @Override
    public void execute() {
        System.out.print("Enter project id: ");
        String projectId = TerminalUtil.nextLine();
        if (!serviceLocator.getProjectTaskService().isProjectId(projectId)) {
            throw new ProjectNotFoundException();
        }
        System.out.print("Enter task id: ");
        String taskId = TerminalUtil.nextLine();
        if (!serviceLocator.getProjectTaskService().isTaskId(taskId)) {
            throw new TaskNotFoundException();
        }
        serviceLocator.getProjectTaskService().unbindTaskByProjectId(projectId, taskId);
    }

}
