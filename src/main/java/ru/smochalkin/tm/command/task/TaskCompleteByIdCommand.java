package ru.smochalkin.tm.command.task;

import ru.smochalkin.tm.command.AbstractTaskCommand;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.util.TerminalUtil;

public final class TaskCompleteByIdCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-complete-by-id";
    }

    @Override
    public String description() {
        return "Complete task by id.";
    }

    @Override
    public void execute() {
        System.out.print("Enter id: ");
        String id = TerminalUtil.nextLine();
        serviceLocator.getTaskService().findById(id);
        serviceLocator.getTaskService().updateStatusById(id, Status.COMPLETED);
    }

}
