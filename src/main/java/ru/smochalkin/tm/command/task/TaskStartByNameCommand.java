package ru.smochalkin.tm.command.task;

import ru.smochalkin.tm.command.AbstractTaskCommand;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.util.TerminalUtil;

public final class TaskStartByNameCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-start-by-name";
    }

    @Override
    public String description() {
        return "Start task by name.";
    }

    @Override
    public void execute() {
        System.out.print("Enter name: ");
        String name = TerminalUtil.nextLine();
        serviceLocator.getTaskService().findByName(name);
        serviceLocator.getTaskService().updateStatusByName(name, Status.IN_PROGRESS);
    }

}
