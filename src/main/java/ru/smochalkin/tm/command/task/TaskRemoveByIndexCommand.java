package ru.smochalkin.tm.command.task;

import ru.smochalkin.tm.command.AbstractTaskCommand;
import ru.smochalkin.tm.util.TerminalUtil;

public final class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-remove-by-index";
    }

    @Override
    public String description() {
        return "Remove task by index.";
    }

    @Override
    public void execute() {
        System.out.print("Enter index: ");
        Integer index = TerminalUtil.nextInt();
        serviceLocator.getTaskService().removeByIndex(--index);
    }

}
