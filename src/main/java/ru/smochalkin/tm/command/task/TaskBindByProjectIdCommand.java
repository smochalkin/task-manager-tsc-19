package ru.smochalkin.tm.command.task;

import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.exception.entity.ProjectNotFoundException;
import ru.smochalkin.tm.exception.entity.TaskNotFoundException;
import ru.smochalkin.tm.util.TerminalUtil;

public final class TaskBindByProjectIdCommand extends AbstractCommand {

    @Override
    public String name() {
        return "task-bind-by-project-id";
    }

    @Override
    public String description() {
        return "Bind task to project by id.";
    }

    @Override
    public void execute() {
        System.out.print("Enter project id: ");
        String projectId = TerminalUtil.nextLine();
        if (!serviceLocator.getProjectTaskService().isProjectId(projectId)) {
            throw new ProjectNotFoundException();
        }
        System.out.print("Enter task id: ");
        String taskId = TerminalUtil.nextLine();
        if (!serviceLocator.getProjectTaskService().isTaskId(taskId)) {
            throw new TaskNotFoundException();
        }
        serviceLocator.getProjectTaskService().bindTaskByProjectId(projectId, taskId);
    }

}
