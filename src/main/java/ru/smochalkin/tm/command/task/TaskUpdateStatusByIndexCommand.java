package ru.smochalkin.tm.command.task;

import ru.smochalkin.tm.command.AbstractTaskCommand;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.exception.system.StatusNotFoundException;
import ru.smochalkin.tm.model.Task;
import ru.smochalkin.tm.util.TerminalUtil;

import java.util.Arrays;

public final class TaskUpdateStatusByIndexCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-status-update-by-index";
    }

    @Override
    public String description() {
        return "Update task status by index.";
    }

    @Override
    public void execute() {
        System.out.print("Enter index: ");
        Integer index = TerminalUtil.nextInt();
        index--;
        serviceLocator.getTaskService().findByIndex(index);
        System.out.println("Enter new status from list:");
        System.out.println(Arrays.toString(Status.values()));
        String statusName = TerminalUtil.nextLine();
        Status status;
        try {
            status = Status.valueOf(statusName);
        } catch (IllegalArgumentException e) {
            throw new StatusNotFoundException();
        }
        serviceLocator.getTaskService().updateStatusByIndex(index, status);
    }

}
