package ru.smochalkin.tm.command.task;

import ru.smochalkin.tm.command.AbstractTaskCommand;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.exception.system.StatusNotFoundException;
import ru.smochalkin.tm.util.TerminalUtil;

import java.util.Arrays;

public final class TaskUpdateStatusByIdCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-status-update-by-id";
    }

    @Override
    public String description() {
        return "Update task status by id.";
    }

    @Override
    public void execute() {
        System.out.print("Enter id: ");
        String id = TerminalUtil.nextLine();
        serviceLocator.getTaskService().findById(id);
        System.out.println("Enter new status from list:");
        System.out.println(Arrays.toString(Status.values()));
        String statusName = TerminalUtil.nextLine();
        Status status;
        try {
            status = Status.valueOf(statusName);
        } catch (IllegalArgumentException e) {
            throw new StatusNotFoundException();
        }
        serviceLocator.getTaskService().updateStatusById(id, status);
    }

}
