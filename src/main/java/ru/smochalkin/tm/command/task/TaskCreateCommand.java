package ru.smochalkin.tm.command.task;

import ru.smochalkin.tm.command.AbstractTaskCommand;
import ru.smochalkin.tm.util.TerminalUtil;

public final class TaskCreateCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-create";
    }

    @Override
    public String description() {
        return "Create new task.";
    }

    @Override
    public void execute() {
        System.out.println("[CREATE TASK]");
        System.out.print("Enter name: ");
        String name = TerminalUtil.nextLine();
        System.out.print("Enter description: ");
        String description = TerminalUtil.nextLine();
        serviceLocator.getTaskService().create(name, description);
        System.out.println("[OK]");
    }

}
