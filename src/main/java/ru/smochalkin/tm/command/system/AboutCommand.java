package ru.smochalkin.tm.command.system;

import ru.smochalkin.tm.command.AbstractSystemCommand;

public final class AboutCommand extends AbstractSystemCommand {

    @Override
    public String arg() {
        return "-a";
    }

    @Override
    public String name() {
        return "about";
    }

    @Override
    public String description() {
        return "Display developer info.";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Developer: Sergey Mochalkin");
        System.out.println("smochalkin@gmail.com");
    }

}
