package ru.smochalkin.tm.api.repository;

import ru.smochalkin.tm.api.IRepository;
import ru.smochalkin.tm.model.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    User removeUser(User user);

    User findByLogin(String login);

    User removeByLogin(String login);

    Boolean isLogin(String login);

}
