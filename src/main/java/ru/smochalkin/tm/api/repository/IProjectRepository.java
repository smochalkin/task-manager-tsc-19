package ru.smochalkin.tm.api.repository;

import ru.smochalkin.tm.api.IRepository;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    Project findByName(String name);

    Project findByIndex(int index);

    Project removeByName(String name);

    Project removeByIndex(Integer index);

    Project updateById(String id, String name, String desc);

    Project updateByIndex(Integer index, String name, String desc);

    Project updateStatusById(String id, Status status);

    Project updateStatusByName(String name, Status status);

    Project updateStatusByIndex(int index, Status status);

}
