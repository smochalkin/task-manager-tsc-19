package ru.smochalkin.tm.api;

import ru.smochalkin.tm.model.AbstractEntity;

import java.util.Comparator;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    void add(E entity);

    void clear();

    List<E> findAll(Comparator<E> comparator);

    List<E> findAll();

    E findById(String id);

    E remove(E entity);

    E removeById(String id);

    int getCount();

}