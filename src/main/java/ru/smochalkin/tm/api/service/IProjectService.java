package ru.smochalkin.tm.api.service;

import ru.smochalkin.tm.api.IService;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.model.Project;

public interface IProjectService extends IService<Project> {

    Project create(String name, String description);

    Project findByName(String name);

    Project findByIndex(Integer index);

    Project removeByName(String name);

    Project removeByIndex(Integer index);

    Project updateById(String id, String name, String desc);

    Project updateByIndex(Integer index, String name, String desc);

    Project updateStatusById(String id, Status status);

    Project updateStatusByName(String name, Status status);

    Project updateStatusByIndex(Integer index, Status status);

    boolean isIndex(Integer index);

}
