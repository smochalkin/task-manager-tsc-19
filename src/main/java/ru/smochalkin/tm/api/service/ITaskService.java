package ru.smochalkin.tm.api.service;

import ru.smochalkin.tm.api.IService;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.model.Task;

public interface ITaskService extends IService<Task> {

    Task create(String name, String description);

    Task findByName(String name);

    Task findByIndex(Integer index);

    Task removeByName(String name);

    Task removeByIndex(Integer index);

    Task updateById(String id, String name, String desc);

    Task updateByIndex(Integer index, String name, String desc);

    Task updateStatusById(String id, Status status);

    Task updateStatusByName(String name, Status status);

    Task updateStatusByIndex(Integer index, Status status);

    boolean isIndex(Integer index);

}
