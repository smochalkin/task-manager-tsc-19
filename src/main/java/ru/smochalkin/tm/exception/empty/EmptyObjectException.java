package ru.smochalkin.tm.exception.empty;

import ru.smochalkin.tm.exception.AbstractException;

public final class EmptyObjectException extends AbstractException {

    public EmptyObjectException() {
        super("Error! Email is empty...");
    }

}
