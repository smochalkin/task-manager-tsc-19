package ru.smochalkin.tm.repository;

import ru.smochalkin.tm.api.repository.IProjectRepository;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.exception.entity.ProjectNotFoundException;
import ru.smochalkin.tm.model.Project;

import java.util.Date;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public Project findByName(final String name) {
        for (Project project : list) {
            if (project.getName().equals(name)) return project;
        }
        throw new ProjectNotFoundException();
    }

    @Override
    public Project findByIndex(final int index) {
        return list.get(index);
    }

    @Override
    public Project removeByName(final String name) {
        Project project = findByName(name);
        list.remove(project);
        return project;
    }

    @Override
    public Project removeByIndex(final Integer index) {
        Project project = findByIndex(index);
        list.remove(project);
        return project;
    }

    @Override
    public Project updateById(final String id, final String name, final String desc) {
        Project project = findById(id);
        project.setName(name);
        project.setDescription(desc);
        return project;
    }

    @Override
    public Project updateByIndex(final Integer index, final String name, final String desc) {
        Project project = findByIndex(index);
        project.setName(name);
        project.setDescription(desc);
        return project;
    }

    @Override
    public Project updateStatusById(final String id, final Status status) {
        Project project = findById(id);
        project.setStatus(status);
        updateDate(project, status);
        return project;
    }

    @Override
    public Project updateStatusByName(final String name, final Status status) {
        Project project = findByName(name);
        project.setStatus(status);
        updateDate(project, status);
        return project;
    }

    @Override
    public Project updateStatusByIndex(final int index, final Status status) {
        Project project = findByIndex(index);
        project.setStatus(status);
        updateDate(project, status);
        return project;
    }

    private void updateDate(final Project project, final Status status){
        switch (status){
            case IN_PROGRESS: project.setStartDate(new Date()); break;
            case COMPLETED: project.setEndDate(new Date()); break;
            case NOT_STARTED: project.setStartDate(null); project.setEndDate(null);
        }
    }

}
