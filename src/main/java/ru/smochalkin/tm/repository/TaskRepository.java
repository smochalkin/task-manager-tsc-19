package ru.smochalkin.tm.repository;

import ru.smochalkin.tm.api.repository.ITaskRepository;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.exception.entity.TaskNotFoundException;
import ru.smochalkin.tm.model.Task;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public final class TaskRepository  extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public Task findByName(final String name) {
        for (Task task : list) {
            if (task.getName().equals(name)) return task;
        }
        throw new TaskNotFoundException();
    }

    @Override
    public Task findByIndex(final int index) {
        return list.get(index);
    }

    @Override
    public Task removeByName(final String name) {
        Task task = findByName(name);
        list.remove(task);
        return task;
    }

    @Override
    public Task removeByIndex(final Integer index) {
        Task task = findByIndex(index);
        list.remove(task);
        return task;
    }

    @Override
    public Task updateById(final String id, final String name, final String desc) {
        Task task = findById(id);
        task.setName(name);
        task.setDescription(desc);
        return task;
    }

    @Override
    public Task updateByIndex(final Integer index, final String name, final String desc) {
        Task task = findByIndex(index);
        task.setName(name);
        task.setDescription(desc);
        return task;
    }

    @Override
    public Task updateStatusById(final String id, final Status status) {
        Task task = findById(id);
        task.setStatus(status);
        updateDate(task, status);
        return task;
    }

    @Override
    public Task updateStatusByName(final String name, final Status status) {
        Task task = findByName(name);
        task.setStatus(status);
        updateDate(task, status);
        return task;
    }

    @Override
    public Task updateStatusByIndex(final int index, final Status status) {
        Task task = findByIndex(index);
        task.setStatus(status);
        updateDate(task, status);
        return task;
    }


    private void updateDate(final Task task, final Status status){
        switch (status){
            case IN_PROGRESS: task.setStartDate(new Date()); break;
            case COMPLETED: task.setEndDate(new Date()); break;
            case NOT_STARTED: task.setStartDate(null); task.setEndDate(null);
        }
    }

    @Override
    public Task bindTaskById(final String projectId, final String taskId) {
        Task task = findById(taskId);
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindTaskById(final String taskId) {
        Task task = findById(taskId);
        task.setProjectId(null);
        return task;
    }

    @Override
    public List<Task> findTasksByProjectId(final String projectId) {
        List<Task> result = new ArrayList<>();
        for (Task task : list) {
            if (projectId.equals(task.getProjectId())) result.add(task);
        }
        return result;
    }

    @Override
    public void removeTasksByProjectId(final String projectId) {
        List<Task> projectTaskList = findTasksByProjectId(projectId);
        list.removeAll(projectTaskList);
    }

}