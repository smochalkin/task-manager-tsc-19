package ru.smochalkin.tm.service;

import ru.smochalkin.tm.api.IRepository;
import ru.smochalkin.tm.api.IService;
import ru.smochalkin.tm.exception.empty.EmptyIdException;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.model.AbstractEntity;

import java.util.Comparator;
import java.util.List;

import static ru.smochalkin.tm.util.ValidateUtil.isEmpty;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    protected final IRepository<E> repository;

    public AbstractService(final IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @Override
    public List<E> findAll(final Comparator<E> comparator) {
        return repository.findAll(comparator);
    }

    @Override
    public E findById(final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        return repository.findById(id);
    }

    @Override
    public E removeById(final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        return repository.removeById(id);
    }

    @Override
    public void add(final E entity) {
        if (entity == null) throw new EmptyObjectException();
        repository.add(entity);
    }

    @Override
    public E remove(final E entity) {
        if (entity == null) throw new EmptyObjectException();
        return repository.remove(entity);
    }

    @Override
    public int getCount() {
        return repository.getCount();
    }

}
