package ru.smochalkin.tm.service;

import ru.smochalkin.tm.api.repository.IUserRepository;
import ru.smochalkin.tm.api.service.IUserService;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.exception.empty.*;
import ru.smochalkin.tm.exception.system.LoginExistsException;
import ru.smochalkin.tm.model.User;
import ru.smochalkin.tm.util.HashUtil;

import static ru.smochalkin.tm.util.ValidateUtil.isEmpty;

public final class UserService extends AbstractService<User> implements IUserService {

    private final IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @Override
    public User findByLogin(final String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    public User removeByLogin(final String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        return userRepository.removeByLogin(login);
    }

    @Override
    public void create(final String login, final String password) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isLogin(login)) throw new LoginExistsException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        userRepository.add(user);
    }

    @Override
    public void create(final String login, final String password, final String email) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isLogin(login)) throw new LoginExistsException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        if (isEmpty(email)) throw new EmptyEmailException();
        User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setEmail(email);
        userRepository.add(user);
    }

    @Override
    public void create(final String login, final String password, final Role role) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isLogin(login)) throw new LoginExistsException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setRole(role);
        userRepository.add(user);
    }

    @Override
    public User setPassword(final String userId, final String password) {
        if (isEmpty(userId)) throw new EmptyIdException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        User user = findById(userId);
        user.setPasswordHash(HashUtil.salt(password));
        return user;
    }

    @Override
    public User updateById(final String id, final String firstName, final String lastName, final String middleName) {
        if (isEmpty(id)) throw new EmptyIdException();
        User user = findById(id);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @Override
    public boolean isLogin(final String login) {
        return userRepository.isLogin(login);
    }

}

