package ru.smochalkin.tm.service;

import ru.smochalkin.tm.api.repository.ITaskRepository;
import ru.smochalkin.tm.api.service.ITaskService;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.exception.empty.EmptyIdException;
import ru.smochalkin.tm.exception.empty.EmptyNameException;
import ru.smochalkin.tm.exception.system.IndexIncorrectException;
import ru.smochalkin.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Override
    public Task create(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
        return task;
    }

    @Override
    public Task findByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.findByName(name);
    }

    @Override
    public Task findByIndex(final Integer index) {
        if(!isIndex(index)) throw new IndexIncorrectException();
        return taskRepository.findByIndex(index);
    }

    @Override
    public Task removeByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.removeByName(name);
    }

    @Override
    public Task removeByIndex(final Integer index) {
        if(!isIndex(index)) throw new IndexIncorrectException();
        return taskRepository.removeByIndex(index);
    }

    @Override
    public Task updateById(final String id, final String name, final String desc) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.updateById(id, name, desc);
    }

    @Override
    public Task updateByIndex(final Integer index, final String name, final String desc) {
        if(!isIndex(index)) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.updateByIndex(index, name, desc);
    }

    @Override
    public Task updateStatusById(final String id, final Status status) {
        return taskRepository.updateStatusById(id, status);
    }

    @Override
    public Task updateStatusByName(final String name, final Status status) {
        return taskRepository.updateStatusByName(name, status);
    }

    @Override
    public Task updateStatusByIndex(final Integer index, final Status status) {
        return taskRepository.updateStatusByIndex(index, status);
    }

    @Override
    public boolean isIndex(final Integer index) {
        if (index == null || index < 0) return false;
        if (index >= taskRepository.getCount()) return false;
        return true;
    }

}
