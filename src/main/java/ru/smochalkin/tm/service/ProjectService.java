package ru.smochalkin.tm.service;

import ru.smochalkin.tm.api.repository.IProjectRepository;
import ru.smochalkin.tm.api.service.IProjectService;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.exception.empty.EmptyIdException;
import ru.smochalkin.tm.exception.empty.EmptyNameException;
import ru.smochalkin.tm.exception.system.IndexIncorrectException;
import ru.smochalkin.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }

    @Override
    public Project create(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
        return project;
    }

    @Override
    public Project findByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.findByName(name);
    }

    @Override
    public Project findByIndex(final Integer index) {
        if(!isIndex(index)) throw new IndexIncorrectException();
        return projectRepository.findByIndex(index);
    }

    @Override
    public Project removeByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.removeByName(name);
    }

    @Override
    public Project removeByIndex(final Integer index) {
        if(!isIndex(index)) throw new IndexIncorrectException();
        return projectRepository.removeByIndex(index);
    }

    @Override
    public Project updateById(final String id, final String name, final String desc) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.updateById(id, name, desc);
    }

    @Override
    public Project updateByIndex(final Integer index, final String name, final String desc) {
        if(!isIndex(index)) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.updateByIndex(index, name, desc);
    }

    @Override
    public Project updateStatusById(final String id, final Status status) {
        return projectRepository.updateStatusById(id, status);
    }

    @Override
    public Project updateStatusByName(final String name, final Status status) {
        return projectRepository.updateStatusByName(name, status);
    }

    @Override
    public Project updateStatusByIndex(final Integer index, final Status status) {
        return projectRepository.updateStatusByIndex(index, status);
    }

    @Override
    public boolean isIndex(final Integer index) {
        if (index == null || index < 0) return false;
        if (index >= projectRepository.getCount()) return false;
        return true;
    }

}
