package ru.smochalkin.tm.service;

import ru.smochalkin.tm.api.repository.IProjectRepository;
import ru.smochalkin.tm.api.repository.ITaskRepository;
import ru.smochalkin.tm.exception.empty.EmptyIdException;
import ru.smochalkin.tm.exception.empty.EmptyNameException;
import ru.smochalkin.tm.model.Project;
import ru.smochalkin.tm.model.Task;

import java.util.List;

public final class ProjectTaskService implements ru.smochalkin.tm.api.service.IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(final IProjectRepository projectRepository, final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public Task bindTaskByProjectId(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
        projectRepository.findById(projectId);
        taskRepository.findById(taskId);
        return taskRepository.bindTaskById(projectId, taskId);
    }

    @Override
    public Task unbindTaskByProjectId(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
        projectRepository.findById(projectId);
        taskRepository.findById(taskId);
        return taskRepository.unbindTaskById(taskId);
    }

    @Override
    public List<Task> findTasksByProjectId(final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        return taskRepository.findTasksByProjectId(projectId);
    }

    @Override
    public void removeProjectById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        taskRepository.removeTasksByProjectId(id);
        projectRepository.removeById(id);
    }

    @Override
    public void removeProjectByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        Project project = projectRepository.findByName(name);
        removeProjectById(project.getId());
    }

    @Override
    public void removeProjectByIndex(final Integer index) {
        Project project = projectRepository.findByIndex(index);
        removeProjectById(project.getId());
    }

    @Override
    public boolean isProjectId(final String id) {
        return projectRepository.findById(id) != null;
    }

    @Override
    public boolean isProjectName(final String name) {
        return projectRepository.findByName(name) != null;
    }

    @Override
    public boolean isProjectIndex(final Integer index) {
        if (index == null || index < 0) return false;
        if (index >= projectRepository.getCount()) return false;
        return true;
    }

    @Override
    public boolean isTaskId(final String id) {
        return taskRepository.findById(id) != null;
    }

}
