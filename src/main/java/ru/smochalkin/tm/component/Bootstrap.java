package ru.smochalkin.tm.component;

import ru.smochalkin.tm.api.repository.ICommandRepository;
import ru.smochalkin.tm.api.repository.IProjectRepository;
import ru.smochalkin.tm.api.repository.ITaskRepository;
import ru.smochalkin.tm.api.repository.IUserRepository;
import ru.smochalkin.tm.api.service.*;
import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.command.system.*;
import ru.smochalkin.tm.command.project.*;
import ru.smochalkin.tm.command.task.*;
import ru.smochalkin.tm.command.user.*;
import ru.smochalkin.tm.constant.TerminalConst;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.exception.system.UnknownCommandException;
import ru.smochalkin.tm.repository.CommandRepository;
import ru.smochalkin.tm.repository.ProjectRepository;
import ru.smochalkin.tm.repository.TaskRepository;
import ru.smochalkin.tm.repository.UserRepository;
import ru.smochalkin.tm.service.*;
import ru.smochalkin.tm.util.TerminalUtil;


public class Bootstrap implements ServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final ILogService logService = new LogService();

    public final IUserRepository userRepository = new UserRepository();

    public final IUserService userService = new UserService(userRepository);

    public final IAuthService authService = new AuthService(userService);

    private void initData() {
        projectService.create("ProjectB", "lorem ipsum");
        projectService.create("ProjectD", "lorem ipsum").setStatus(Status.IN_PROGRESS);
        projectService.create("ProjectA", "lorem ipsum").setStatus(Status.COMPLETED);
        projectService.create("ProjectC", "lorem ipsum").setStatus(Status.IN_PROGRESS);
        taskService.create("TaskB", "lorem ipsum");
        taskService.create("TaskA", "lorem ipsum").setStatus(Status.COMPLETED);
        taskService.create("TaskC", "lorem ipsum").setStatus(Status.IN_PROGRESS);
        taskService.create("TaskD", "lorem ipsum").setStatus(Status.COMPLETED);
        userService.create("user", "1", "user@user.ru");
        userService.create("admin", "100", Role.ADMIN);
    }

    {
        registry(new AboutCommand());
        registry(new ArgumentListCommand());
        registry(new CommandListCommand());
        registry(new ExitCommand());
        registry(new HelpCommand());
        registry(new InfoCommand());
        registry(new VersionCommand());
        registry(new ProjectUpdateStatusByIdCommand());
        registry(new ProjectUpdateStatusByIndexCommand());
        registry(new ProjectUpdateStatusByNameCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectCompleteByNameCommand());
        registry(new ProjectShowListCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectRemoveByNameCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByNameCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectStartByNameCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new TaskBindByProjectIdCommand());
        registry(new TaskUpdateStatusByIdCommand());
        registry(new TaskUpdateStatusByIndexCommand());
        registry(new TaskUpdateStatusByNameCommand());
        registry(new TaskClearCommand());
        registry(new TaskCreateCommand());
        registry(new TaskShowListByProjectIdCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskCompleteByNameCommand());
        registry(new TaskShowListCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByNameCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByNameCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskStartByNameCommand());
        registry(new TaskUnbindByProjectIdCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserRegistryCommand());
        registry(new UserIsAuthCommand());
        registry(new UserChangePassCommand());
        registry(new UserUpdateCommand());
        registry(new UserShowCommand());
        registry(new UserRemoveByIdCommand());
        registry(new UserRemoveByLoginCommand());

    }

    public void start(final String[] args) {
        System.out.println("** WELCOME TO TASK MANAGER **");
        initData();
        parseArgs(args);
        process();
    }

    public void parseArgs(final String[] args) {
        if (args == null || args.length == 0) return;
        String arg = args[0];
        parseArg(arg);
        System.exit(0);
    }

    public void parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        AbstractCommand command = commandService.getCommandByArg(arg);
        if (command == null) return;
        command.execute();
    }

    public void process() {
        logService.debug("process start...");
        String command = "";
        while (!TerminalConst.EXIT.equals(command)) {
            System.out.println("Enter command: ");
            command = TerminalUtil.nextLine();
            logService.command(command);
            try {
                parseCommand(command);
                logService.info("`" + command + "` command executed");
            } catch (Exception e) {
                logService.error(e);
            }
            System.out.println();
        }
    }

    public void parseCommand(final String cmd) {
        if (cmd == null || cmd.isEmpty()) return;
        AbstractCommand command = commandService.getCommandByName(cmd);
        if (command == null) throw new UnknownCommandException();
        command.execute();
    }

    public void registry(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

}
