package ru.smochalkin.tm.model;

import ru.smochalkin.tm.api.model.IWBS;
import ru.smochalkin.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

public final class Project extends AbstractEntity implements IWBS {

    private String name;

    private String description;

    private Status status = Status.NOT_STARTED;

    private final Date created = new Date();

    private Date startDate;

    private Date endDate;

    public Project() {
    }

    public Project(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Date getCreated() {
        return created;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return id + ": " + name + ": " + description +
                "; Status - " + status.getDisplayName() +
                "; Created - " + created +
                "; Start - " + startDate +
                "; End - " + endDate;
    }

}
